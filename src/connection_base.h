#ifndef CONNECTION_BASE_HPP
#define CONNECTION_BASE_HPP

namespace connection
{

class E_connection
{
};

class connection_base
{            
    public:
        // connection is ok
        virtual bool ok() = 0;
		
		// Write nbr_chars, or less if timeout occurs
		// Return the number of written chars.
		virtual int write( const char* buffer, int nbr_chars ) = 0;
		
		// Read nbr_chars_wanted, or less if timeout occurs
		// Return the number of read chars.
		virtual int read( char* buffer, int nbr_chars_wanted ) = 0;

        // set read timeout, milliseconds
        virtual void set_read_timeout( int timeout ) = 0;
             
        // set write timeout, milliseconds
        virtual void set_write_timeout( int timeout ) = 0;
};
    
}

#endif //CONNECTION_BASE_HPP



