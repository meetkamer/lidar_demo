#ifndef U_EXCEPTION_HPP
#define U_EXCEPTION_HPP

#include <string>
//#include "U_log_base.hpp" 
class log_object
{
public:
	virtual void destroy() = 0;
	virtual void set_level(int level) = 0;
	// Message will be ignored if level > set_level
	// So messages with the highest priority have level 0
	virtual void log(int level, const char* source_file, int source_line, const char* format, ...)
	{
		va_list ap;
		va_start(ap, format);
		_log(level, source_file, source_line, format, ap);
		va_end(ap);
	}
	virtual void _log(int level, const char* source_file, int source_line, const char* format, va_list ap) = 0;
};

template <typename exception_class>
void U_throw( const char* file, int line, const char* format, ...)
{
    exception_class e;
    va_list ap;
	va_start(ap, format);
    e.set_info( file, line, format, ap);
    va_end(ap);
    throw e;
}

#define U_THROW( exception_class, format, ...) do { \
    U_throw<exception_class>( __FILE__, __LINE__, format, ##__VA_ARGS__);\
} while(0)

#ifdef _MSC_VER
#define U_THROW_LAST_ERROR(t, format, ...)\
    U_throw<t>( __FILE__, __LINE__, format ". GetLastError() returns %d", ##__VA_ARGS__, GetLastError());
    //U_THROW(t, format ". GetLastError() returns %d", ##__VA_ARGS__, GetLastError());
#endif

class U_exception
{
private:
    static log_object* _static_log_object;
public:
    std::string _file;
    int _line;
    std::string _message;
    static void register_static_log_object(log_object* log_object)
    {
        _static_log_object = log_object;
    }
    U_exception()
    {
    }
    void set_info( const char* file, int line, const char* format, ... )
    {
        va_list ap;
		va_start(ap, format);
        set_info( file, line, format, ap);
        va_end(ap);
    }
    void set_info( const char* file, int line, const char* format, va_list ap )
    {
        _file = file;
        _line = line;
        const int buf_size = 512;
        char buffer[buf_size];
        _vsnprintf( buffer, buf_size, format, ap);
        _message = buffer;
        if(_static_log_object)
            _static_log_object->_log(0, file, line, format, ap);
    }
};

#endif


