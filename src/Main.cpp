//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#include "stdafx.h"

#include "D3D12nBodyGravity.h"

char  lidar_com_port[64] = "COM1";
float lidar_rotation_speed;
float lidar_xrange;
float lidar_yrange;
float lidar_zrange;
float lidar_speed;


#ifndef LIDAR_LIB
_Use_decl_annotations_
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int nCmdShow)
{
	D3D12nBodyGravity sample(1280, 720, L"Intec Lidar Demo");
	return Win32Application::Run(&sample, hInstance, nCmdShow);
}
#else

HANDLE hthread;
DWORD thread_id;

DWORD WINAPI lidar_thread(LPVOID lpParameter)
{
    HINSTANCE hInstance = GetModuleHandle(NULL);
    int nCmdShow = 10;
    D3D12nBodyGravity sample(1280, 720, L"Intec LIDAR");
    return Win32Application::Run(&sample, hInstance, nCmdShow);
}

int lidar_lib_begin_thread()
{
    hthread = CreateThread(
        NULL,          // default security attributes
        0,             // use default stack size  
        lidar_thread,  // thread function name
        nullptr,          // argument to thread function 
        0,             // use default creation flags 
        &thread_id);   // returns the thread identifier 
    return 0;
}

void lidar_lib_send_close()
{
    HWND hwnd = Win32Application::GetHwnd();
    SendMessage(hwnd, WM_CLOSE, 0, 0);
    WaitForSingleObjectEx(hthread, INFINITE, FALSE);
}

#endif

