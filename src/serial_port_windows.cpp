#include "stdafx.h"

#include "connection_base.h"
#include "serial_port.h"

//#include "ComPort.h"

#include "comPortWin.hpp"

namespace connection
{
namespace serial
{
    serial_port::serial_port(
         const char* port,                    
         BAUDRATES baudrate,         
         BYTESIZES bytesize,      
         PARITIES parity,        
         STOPBITS stopbits,  
         int read_timeout,                  
         int write_timeout              
        ) 
    {
        _open = false;
        _configured = false;
        _com = 0;
        // !! to do: convert BYTESIZES, PARITIES, STOPBITS to win32 equivalents
        // for now, the defaults conveniently match our application
        comPortWin* com = 0;
        try
        {
            com = new comPortWin( port, baudrate /*,...*/ );
		    _com = (void*)com;
            _open = true;
		    com->setReadTimeout(read_timeout);
		    com->setWriteTimeout(write_timeout);
        }
        catch (U_exception &)
        {
            cleanup();
            throw;
        }
        _configured = true;
    }   

    serial_port::~serial_port()
    {
        cleanup();
    }

    void serial_port::cleanup()
    {
		if(_com)
			delete (comPortWin*)_com;
        _com = 0;
    }

    int serial_port::write( const char* buffer, int total_wanted )
    {
        int total_written = -1;
        total_written = ((comPortWin*)_com)->write( buffer, total_wanted );
        return total_written;
    }
    
    int serial_port::read( char* buffer, int max_read )
    {        
        int total_read = -1;
        total_read = ((comPortWin*)_com)->read( buffer, max_read );
        return total_read;
    }

	// set read timeout, milliseconds
	void serial_port::set_read_timeout( int timeout )
	{
       	((comPortWin*)_com)->setReadTimeout( timeout );
	}

	// set write timeout, milliseconds
	void serial_port::set_write_timeout( int timeout )
	{
   		((comPortWin*)_com)->setWriteTimeout( timeout );
	}    

}   // namespace serial        
}	// namespace connection


log_object* U_exception::_static_log_object;
