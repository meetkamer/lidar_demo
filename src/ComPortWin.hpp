#ifndef EXTENDED_SERIAL_PORT_WINDOWS_HPP
#define EXTENDED_SERIAL_PORT_WINDOWS_HPP
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <string>
#include "U_exception.h"



class E_comport_win : public U_exception
{
};


class comPortWin
{
private:
    HANDLE _hcom;

    std::string _name;
    DWORD _baudrate;
    WORD _bytesize;
    WORD _parity;
    WORD _stopbits;
    int _readTimeout;
    int _writeTimeout;
    bool _xonxoff;
    bool _rtscts;
    bool _dsrdtr;
    int _interCharTimeout;
    
    unsigned char _rtsState;
    unsigned char _dtrState;

    void open()
    {
        _hcom = CreateFileA(_name.c_str(),
            GENERIC_READ | GENERIC_WRITE,
            0, // exclusive access
            0, // no security
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            0);
        if( INVALID_HANDLE_VALUE == _hcom )
        {
            U_THROW_LAST_ERROR(E_comport_win, "CreateFile on %s failed", _name.c_str() );
        }
        if( !SetupComm(_hcom, 4096, 4096) )
        {
            U_THROW_LAST_ERROR(E_comport_win, "SetupComm on %s failed", _name.c_str() );
        }
        _rtsState = RTS_CONTROL_ENABLE;
        _dtrState = DTR_CONTROL_ENABLE;
        reconfigure();
        if (!PurgeComm(_hcom, PURGE_TXCLEAR | PURGE_TXABORT | PURGE_RXCLEAR | PURGE_RXABORT))
        {
            U_THROW_LAST_ERROR(E_comport_win, "PurgeComm on %s failed", _name.c_str() );
        }
    }

    void setTimeouts()
    {
        COMMTIMEOUTS timeouts;
        int NbrMilliseconds = _readTimeout;
	    if (NbrMilliseconds < 0)
	    {
            // block, except if _interCharTimeout > 0
		    timeouts.ReadIntervalTimeout = 0;
		    timeouts.ReadTotalTimeoutMultiplier = 0;
		    timeouts.ReadTotalTimeoutConstant = 0;
	    }
	    else if (NbrMilliseconds == 0)
	    {
            // poll
		    timeouts.ReadIntervalTimeout = MAXDWORD;
		    timeouts.ReadTotalTimeoutMultiplier = 0;
		    timeouts.ReadTotalTimeoutConstant = 0;
	    }
	    else
	    {
            // timeout
		    timeouts.ReadIntervalTimeout = 0;
		    timeouts.ReadTotalTimeoutMultiplier = 0;
		    timeouts.ReadTotalTimeoutConstant = NbrMilliseconds;
	    }
        if( NbrMilliseconds!= 0 && _interCharTimeout != -1)
        {
            timeouts.ReadIntervalTimeout = _interCharTimeout;
        }

        NbrMilliseconds = _writeTimeout;
	    if (NbrMilliseconds < 0)
	    {
            // block
		    timeouts.WriteTotalTimeoutMultiplier = 0;
		    timeouts.WriteTotalTimeoutConstant = 0;
	    }
	    else if (NbrMilliseconds == 0)
	    {
            // poll
		    timeouts.WriteTotalTimeoutMultiplier = 0;
		    timeouts.WriteTotalTimeoutConstant = 1; //MAXDWORD?
	    }
	    else
	    {
            // timeout
		    timeouts.WriteTotalTimeoutMultiplier = 0;
		    timeouts.WriteTotalTimeoutConstant = NbrMilliseconds;
	    }
	    if (!SetCommTimeouts(_hcom, &timeouts))
	    {
            U_THROW_LAST_ERROR(E_comport_win, "SetCommTimeouts on %s failed", _name.c_str() );
	    }
    }

    void reconfigure()
    {
        setTimeouts();
        SetCommMask(_hcom, EV_ERR );
        DCB dcb;
        dcb.DCBlength = sizeof(dcb);

    	if (!GetCommState(_hcom, &dcb))
	    {
            U_THROW_LAST_ERROR(E_comport_win, "GetCommState on %s failed", _name.c_str() );
        }
        
        dcb.BaudRate = _baudrate;

        if (_bytesize == DATABITS_5)
            dcb.ByteSize = 5;
        else if (_bytesize == DATABITS_6)
            dcb.ByteSize = 6;
        else if (_bytesize == DATABITS_7)
            dcb.ByteSize = 7;
        else if (_bytesize == DATABITS_8)
            dcb.ByteSize = 8;
        else
            U_THROW(E_comport_win, "Unsupported number of data bits: %r", _bytesize);

        if (_parity == PARITY_NONE)
        {
            dcb.Parity   = NOPARITY;
            dcb.fParity  = 0; // Disable Parity Check
        }
        else if (_parity == PARITY_EVEN)
        {
            dcb.Parity   = EVENPARITY;
            dcb.fParity  = 1; // Enable Parity Check
        }
        else if (_parity == PARITY_ODD)
        {
            dcb.Parity   = ODDPARITY;
            dcb.fParity  = 1; // Enable Parity Check
        }
        else if (_parity == PARITY_MARK)
        {
            dcb.Parity   = MARKPARITY;
            dcb.fParity  = 1; // Enable Parity Check
        }
        else if (_parity == PARITY_SPACE)
        {
            dcb.Parity   = SPACEPARITY;
            dcb.fParity  = 1; // Enable Parity Check
        }
        else
            U_THROW(E_comport_win, "Unsupported parity mode: %d", _parity);

        if (_stopbits == STOPBITS_10)
            dcb.StopBits = ONESTOPBIT;
        else if (_stopbits == STOPBITS_15)
            dcb.StopBits = ONE5STOPBITS;
        else if (_stopbits == STOPBITS_20)
            dcb.StopBits = TWOSTOPBITS;
        else
            U_THROW(E_comport_win, "Unsupported number of stop bits: %d", _stopbits);

        dcb.fBinary      = 1; // Enable Binary Transmission
        // Char. w/ Parity-Err are replaced with 0xff (if fErrorChar is set to TRUE)
        if (_rtscts)
            dcb.fRtsControl  = RTS_CONTROL_HANDSHAKE;
        else
            dcb.fRtsControl  = _rtsState;
        if (_dsrdtr)
            dcb.fDtrControl  = DTR_CONTROL_HANDSHAKE;
        else
            dcb.fDtrControl  = _dtrState;
        dcb.fOutxCtsFlow = _rtscts;
        dcb.fOutxDsrFlow = _dsrdtr;
        dcb.fOutX        = _xonxoff;
        dcb.fInX         = _xonxoff;
        dcb.fNull        = 0;
        dcb.fErrorChar   = 0;
        dcb.fAbortOnError= 0;
        dcb.XonChar      = 17;
        dcb.XoffChar     = 19;

        if (!SetCommState(_hcom, &dcb))
        {
            U_THROW_LAST_ERROR(E_comport_win, "SetCommState on %s failed", _name.c_str() );
        }
    }

    void cleanup() throw()
    {
       if( _hcom && (INVALID_HANDLE_VALUE != _hcom) )
       {
            CloseHandle(_hcom);
       }
    }

public:
    int i;

    comPortWin(
        const char* name,
        DWORD baudrate,
        WORD bytesize = DATABITS_8,
        WORD parity = PARITY_NONE,
        WORD stopbits = STOPBITS_10,
        int readTimeout = -1,
        int writeTimeout = -1,
        bool xonxoff = false,
        bool rtscts = false,
        bool dsrdtr = false,
        int interCharTimeout = -1
        ) :
     _hcom(NULL),
     _name(name),
     _baudrate(baudrate),
     _bytesize(bytesize),
     _parity(parity),
     _stopbits(stopbits),
     _readTimeout(readTimeout),
     _writeTimeout(writeTimeout),
     _xonxoff(xonxoff),
     _rtscts(rtscts),
     _dsrdtr(dsrdtr),
     _interCharTimeout(interCharTimeout)
    {
        try
        {
            open();
        }
        catch (U_exception& )
        {
            cleanup();
            char buffer[255];
            _snprintf(buffer, 255, "Com port '%s' does not exist on this system", name);
            MessageBoxA(NULL, buffer, "Troubles!", MB_OK);
            throw;
        }
    }

    ~comPortWin()
    {
        cleanup();
    }

    int write( const char* data, int nbrBytes )
    {
        DWORD nbr_written = 0;
        BOOL result = WriteFile(_hcom, data, nbrBytes, &nbr_written, 0);
        if( !result && (GetLastError() != ERROR_IO_PENDING) )
        {
            U_THROW_LAST_ERROR(E_comport_win, "WriteFile on %s failed", _name.c_str() );
        }
        return int(nbr_written);
    }

    int read(char* data, int nbr_bytes)
    {
        DWORD nbr_read = 0;
        DWORD errors;
        COMSTAT comstat;
        if (!ClearCommError(_hcom, &errors, &comstat))
        {
            U_THROW_LAST_ERROR(E_comport_win, "ClearCommError on %s failed", _name.c_str() );
        }
        BOOL result = ReadFile(_hcom, data, nbr_bytes, &nbr_read, 0);
        if( !result && (GetLastError() != ERROR_IO_PENDING) )
        {
            U_THROW_LAST_ERROR(E_comport_win, "readFile on %s failed", _name.c_str() );
        }
        return int(nbr_read);
    }
    void setReadTimeout( int NbrMilliseconds )
    {
        _readTimeout = NbrMilliseconds;
        setTimeouts();
    }

	void setWriteTimeout( int NbrMilliseconds )
    {
        _writeTimeout = NbrMilliseconds;
        setTimeouts();
    }
};
#endif

