//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#pragma once

#include "DXSample.h"
#include "SimpleCamera.h"
#include "StepTimer.h"
#include "connection_base.h"
#include "serial_port.h"

extern char  lidar_com_port[];
extern float lidar_rotation_speed;
extern float lidar_xrange;
extern float lidar_yrange;
extern float lidar_zrange;
extern float lidar_speed;

using namespace DirectX;

// Note that while ComPtr is used to manage the lifetime of resources on the CPU,
// it has no understanding of the lifetime of resources on the GPU. Apps must account
// for the GPU lifetime of resources to avoid destroying objects that may still be
// referenced by the GPU.
// An example of this can be found in the class method: OnDestroy().
using Microsoft::WRL::ComPtr;

class D3D12nBodyGravity : public DXSample
{
public:
	D3D12nBodyGravity(UINT width, UINT height, std::wstring name);

	virtual void OnInit();
	virtual void OnUpdate();
	virtual void OnRender();
	virtual void OnDestroy();
	virtual void OnKeyDown(UINT8 key);
	virtual void OnKeyUp(UINT8 key);

private:
	static const UINT FrameCount = 2;
	static const UINT ThreadCount = 1;
	static const float ParticleSpread;
	static const UINT MaxParticleCount = 10000;		// The number of particles in the n-body simulation.

	// "Vertex" definition for particles. Triangle vertices are generated 
	// by the geometry shader. Color data will be assigned to those 
	// vertices via this struct.
	struct ParticleVertex
	{
		XMFLOAT4 color;
	};

	// Position and velocity data for the particles in the system.
	// Two buffers full of Particle data are utilized in this sample.
	// The compute thread alternates writing to each of them.
	// The render thread renders using the buffer that is not currently
	// in use by the compute shader.
	struct Particle
	{
		XMFLOAT4 position;
		XMFLOAT4 velocity;
	};

	struct ConstantBufferGS
	{
		XMFLOAT4X4 worldViewProjection;
		XMFLOAT4X4 inverseView;

		// Constant buffers are 256-byte aligned in GPU memory. Padding is added
		// for convenience when computing the struct's size.
		float padding[32];
	};

	struct ConstantBufferCS
	{
		UINT param[4];
		float paramf[4];
	};

	// Pipeline objects.
	CD3DX12_VIEWPORT m_viewport;
	CD3DX12_RECT m_scissorRect;
	ComPtr<IDXGISwapChain3> m_swapChain;
	ComPtr<ID3D12Device> m_device;
	ComPtr<ID3D12Resource> m_renderTargets[FrameCount];
	UINT m_frameIndex;
	ComPtr<ID3D12CommandAllocator> m_commandAllocators[FrameCount];
	ComPtr<ID3D12CommandQueue> m_commandQueue;
	ComPtr<ID3D12RootSignature> m_rootSignature;
	ComPtr<ID3D12RootSignature> m_computeRootSignature;
	ComPtr<ID3D12DescriptorHeap> m_rtvHeap;
	ComPtr<ID3D12DescriptorHeap> m_srvUavHeap;
	UINT m_rtvDescriptorSize;
	UINT m_srvUavDescriptorSize;

	// Asset objects.
	ComPtr<ID3D12PipelineState> m_pipelineState;
	ComPtr<ID3D12PipelineState> m_computeState;
	ComPtr<ID3D12GraphicsCommandList> m_commandList;
	ComPtr<ID3D12Resource> m_vertexBuffer;
	ComPtr<ID3D12Resource> m_vertexBufferUpload;
	D3D12_VERTEX_BUFFER_VIEW m_vertexBufferView;
	
	CRITICAL_SECTION m_NewParticlesCriticalSection;
	std::vector<Particle> m_NewParticles;
	std::vector<Particle> m_ParticleData;
	UINT64 m_TotalParticleCount;
	int m_CurrentParticleCount;
	int m_NewParticleCount;
	int m_ParticleWriteIndex;
	bool m_ClearParticles;
	bool m_RestartScanner;
    bool m_SaveParticles;

	ComPtr<ID3D12Resource> m_particleBuffer0[ThreadCount];
	ComPtr<ID3D12Resource> m_particleBuffer1[ThreadCount];
	ComPtr<ID3D12Resource> m_particleBuffer0Upload[ThreadCount];
	ComPtr<ID3D12Resource> m_particleBuffer1Upload[ThreadCount];
	ComPtr<ID3D12Resource> m_constantBufferGS;
	UINT8* m_pConstantBufferGSData;
	ComPtr<ID3D12Resource> m_constantBufferCS;

	UINT m_srvIndex[ThreadCount];		// Denotes which of the particle buffer resource views is the SRV (0 or 1). The UAV is 1 - srvIndex.
	UINT m_heightInstances;
	UINT m_widthInstances;
	SimpleCamera m_camera;
	StepTimer m_timer;

	// Compute objects.
	ComPtr<ID3D12CommandAllocator> m_computeAllocator[ThreadCount];
	ComPtr<ID3D12CommandQueue> m_computeCommandQueue[ThreadCount];
	ComPtr<ID3D12GraphicsCommandList> m_computeCommandList[ThreadCount];

	// Synchronization objects.
	HANDLE m_swapChainEvent;
	ComPtr<ID3D12Fence> m_renderContextFence;
	UINT64 m_renderContextFenceValue;
	HANDLE m_renderContextFenceEvent;
	UINT64 m_frameFenceValues[FrameCount];

	ComPtr<ID3D12Fence> m_threadFences[ThreadCount];
	volatile HANDLE m_threadFenceEvents[ThreadCount];

	// Thread state.
	LONG volatile m_terminating;
	UINT64 volatile m_renderContextFenceValues[ThreadCount];
	UINT64 volatile m_threadFenceValues[ThreadCount];

	struct ThreadData
	{
		D3D12nBodyGravity* pContext;
		UINT threadIndex;
	};
	ThreadData m_threadData[ThreadCount];
	HANDLE m_threadHandles[ThreadCount];

	connection::serial::serial_port m_serial_port;
	std::string m_serial_input_buffer;

	////////////////////////////////////////////////////////////////////////////////////
	// Indices of the root signature parameters.
	enum GraphicsRootParameters : UINT32
	{
		GraphicsRootCBV = 0,
		GraphicsRootSRVTable,
		GraphicsRootParametersCount
	};

	enum ComputeRootParameters : UINT32
	{
		ComputeRootCBV = 0,
		ComputeRootSRVTable,
		ComputeRootUAVTable,
		ComputeRootParametersCount
	};

	// Indices of shader resources in the descriptor heap.
	enum DescriptorHeapIndex : UINT32
	{
		UavParticlePosVelo0 = 0,
		UavParticlePosVelo1 = UavParticlePosVelo0 + ThreadCount,
		SrvParticlePosVelo0 = UavParticlePosVelo1 + ThreadCount,
		SrvParticlePosVelo1 = SrvParticlePosVelo0 + ThreadCount,
		DescriptorCount = SrvParticlePosVelo1 + ThreadCount
	};

	void LoadPipeline();
	void LoadAssets();
	void CreateAsyncContexts();
	void CreateVertexBuffer();
	float RandomPercent();
	void LoadParticles(_Out_writes_(numParticles) Particle* pParticles, const XMFLOAT3 &center, const XMFLOAT4 &velocity, float spread, UINT numParticles);
	void CreateParticleBuffers();
	void PopulateCommandList();

	static DWORD WINAPI ThreadProc(ThreadData* pData)
	{
		return pData->pContext->AsyncComputeThreadProc(pData->threadIndex);
	}
	DWORD AsyncComputeThreadProc(int threadIndex);
	void Simulate(UINT threadIndex);

	void WaitForRenderContext();
	void MoveToNextFrame();

	////////////////////////////////////////////////////////////////////////////////
	static DWORD WINAPI _SerialThreadProc(D3D12nBodyGravity* _this)
	{		
		return _this->SerialThreadProc();
	}
	DWORD SerialThreadProc()
	{
		m_serial_port.set_read_timeout(10);
		m_RestartScanner = true;
		bool scan_finished = false;
		for (;;)
		{
			if (m_RestartScanner)
			{
				InitSerial();
				m_RestartScanner = false;
				scan_finished = false;
			}
			for (; !scan_finished;)
			{
				bool end_of_scan = false;
				if (InterlockedCompareExchange(&m_terminating, 0, 0))
				{
					return 0;
				}
				const int buffer_size = 8;
				char buffer[buffer_size + 1];
				int nbr_read = m_serial_port.read(buffer, buffer_size);
				//buffer[nbr_read] = 0;
				//OutputDebugStringA(buffer);
				m_serial_input_buffer.append(buffer, nbr_read);
				//OutputDebugStringA(m_serial_input_buffer.c_str());
				for (;;)
				{
					size_t pos1 = m_serial_input_buffer.find_first_of('<');
					if (pos1 == std::string::npos)
					{
						break;
					}
					size_t pos2 = m_serial_input_buffer.find_first_of('>', pos1);
					if (pos2 == std::string::npos)
					{
						break;
					}
					std::string input = m_serial_input_buffer.substr(pos1 + 1, pos2 - pos1);
					if (input == "end>")
					{
						end_of_scan = true;
					}
					m_serial_input_buffer.erase(0, pos2);
					OutputDebugStringA(input.c_str());
					OutputDebugStringA("\r\n");
					double pol, r, azi;
					{
						// parse format <float;float;float>
						size_t pos1 = input.find_first_of(';');
						if (pos1 == std::string::npos)
						{
							continue;
						}
						std::string num = input.substr(0, pos1);
						azi = atof(num.c_str());
						size_t pos2 = input.find_first_of(';', pos1 + 1);
						num = input.substr(pos1 + 1, pos2 - pos1 - 1);
						r = atof(num.c_str());
						num = input.substr(pos2 + 1, input.length() - pos2 - 1);
						pol = atof(num.c_str());
					}

					Particle P;
					{
						const double pi = 3.1415926535897932384626433832795;
						const double conversion = pi / 180.0;
						azi *= conversion;
						pol *= conversion;
						r *= 5;
						P.position.x = float(r * sin(pol) * cos(azi));
						P.position.z = float(r * sin(pol) * sin(azi));
						P.position.y = float(r * cos(pol));
					}
					EnterCriticalSection(&m_NewParticlesCriticalSection);
					{
						m_NewParticles.push_back(P);
					}
					LeaveCriticalSection(&m_NewParticlesCriticalSection);
				}
				if (end_of_scan)
				{
					scan_finished = true;
					m_SaveParticles = true;
				}
			} // scan finished
            // m_SaveParticles = true;
			Sleep(50);
		}			
		return 0;
	}

	void InitSerial()
	{
		//WriteSerial("<end_ack>");
		Sleep(1000);
		WriteSerial("<KUYKEN>");
		//AskSerial("<ack>");
		WriteSerial("<1>");  // revs per step
		//AskSerial("<ack_rps>");
		WriteSerial("<78>");  // rotation speed
		//AskSerial("<ack_speed>");
		WriteSerial("<26>");  // nbr height steps
		//AskSerial("<ack_steps>");
	}

	void WriteSerial(const char* buffer)
	{
		m_serial_port.write(buffer, int(strlen(buffer)));
	}
	void AskSerial(const char* buffer)
	{
		char input_buffer[256];
		m_serial_port.read(input_buffer, int(strlen(buffer)));
	}
    void SaveParticleFile()
    {
        if (0 == m_CurrentParticleCount)
        {
            //return;
        }
        time_t rawtime;
        struct tm * timeinfo;
        time(&rawtime);
        timeinfo = localtime(&rawtime);
        char filename[256];
        _snprintf(filename, 256, "C:\\lidar_demo\\particle_files\\particles-%02d-%02d-%02d-%02d-%02d.txt", timeinfo->tm_mon, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
        std::ofstream of;
        of.open(filename, std::ofstream::out);

        for (size_t i = 0; i < m_CurrentParticleCount; i++)
        {
            of << m_ParticleData[i].position.x << ',' << m_ParticleData[i].position.y << ',' << m_ParticleData[i].position.z;
            of << ",0,0,0\n";
        }
    }
};
