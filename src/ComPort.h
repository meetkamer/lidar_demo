// ComPort.h: interface for the CComPort class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __COMPORT_H__
#define __COMPORT_H__

#define WIN32_LEAN_AND_MEAN
//#include "atlstr.h"


class CComPort
{
	//CString Name;
	//CString PortNotation(void);

public:
	//int m_debug;

	//CComPort(log_object* p_log_object);
	
	DWORD m_baudrate;  //bevat CBR_9600;
	// DWORD m_parity;
//	BYTE m_parity;
//	DWORD m_DTR;

	void SetBaudRate(long baudrate);

	
	void ClearIncomingTekst(void);
	BOOL IsLineReceived();
	UINT m_IncomingTekstLen;
	char m_IncomingTekst[2048];
	BOOL GetModemChar(LPBYTE pControlChar);
	BOOL GetModemCharInTime(LPBYTE pControlChar,DWORD millisec);
	int Write(const char* pData, DWORD cBytes);
	int Read(char* pData, DWORD cBytes);
	//BOOL Write(LPCSTR pData);
	BOOL DsrOn();
	BOOL CtsOn();
	void ReadErrorAndStatus();
	BOOL CarrierDetect(void);
	BOOL Ringing(void);
	BOOL ModemStatus(DWORD* ModemStat);
	BOOL ZetDTR(BOOL aan);
	BOOL ZetRTS(BOOL aan);
	//void LogLastError(const TCHAR* szDetail);

//	BOOL GetIncomingTekst();
	BOOL m_stringReceived;
	BOOL Open(const char* name);
	BOOL Close(void);
	void WriteCommLines(void);

	int GetPortNr()
	{
		return m_portnr;
	}

	BOOL SetReadTimeout( int NbrMilliseconds );
	BOOL SetWriteTimeout( int NbrMilliseconds );
	void PurgeTx();
	void PurgeRx();
	void PurgeTxRx();

	virtual ~CComPort();

protected:
	void SetTotalTimeout(DWORD TotalTimeOut);
	COMMTIMEOUTS m_ctmo;
	HANDLE m_hCom;

	int m_portnr;  //1 = COM1

};

#endif //__COMPORT_H__
