#pragma once

namespace connection
{

	class E_serial_port : public E_connection
	{
	};

	namespace serial
	{
		enum BAUDRATES {
			BR_0 = 0,
			BR_50 = 50,
			BR_75 = 75,
			BR_110 = 110,
			BR_134 = 134,
			BR_150 = 150,
			BR_200 = 200,
			BR_300 = 300,
			BR_600 = 600,
			BR_1200 = 1200,
			BR_1800 = 1800,
			BR_2400 = 2400,
			BR_4800 = 4800,
			BR_9600 = 9600,
			BR_19200 = 19200,
			BR_38400 = 38400,
			BR_57600 = 57600,
			BR_115200 = 115200,
			BR_230400 = 230400,
			BR_460800 = 460800,
			BR_500000 = 500000,
			BR_576000 = 576000,
			BR_921600 = 921600,
			BR_1000000 = 1000000,
			BR_1152000 = 1152000,
			BR_1500000 = 1500000,
			BR_2000000 = 2000000,
			BR_2500000 = 2500000,
			BR_3000000 = 3000000,
			BR_3500000 = 3500000,
			BR_4000000 = 4000000
		};

		enum BYTESIZES { FIVEBITS = 5, SIXBITS = 6, SEVENBITS = 7, EIGHTBITS = 8 };
		// can't use PARITY_NONE etc.. because they're defined in windows.h
		enum PARITIES { P_NONE = 'N', P_EVEN = 'E', P_ODD = 'O', P_MARK = 'M', P_SPACE = 'S' };
		enum STOPBITS { STOPBITS_ONE = 1, STOPBITS_TWO = 2 };

		class serial_port
		{
		private:
			bool                _open;
			bool                _configured;
#ifdef _MSC_VER
			void*				_com;
#else
			int                 _fd;
			int 				_read_timeout;
			int 				_write_timeout;
#endif
			void cleanup();

		public:
			serial_port(
				log_object* p_log_object,
				const char* port,                     // port name
				BAUDRATES baudrate = BR_9600,         // baud rate
				BYTESIZES bytesize = EIGHTBITS,       // number of data bits
				PARITIES parity = P_NONE,             // enable parity checking
				STOPBITS stopbits = STOPBITS_ONE,     // number of stop bits
				int read_timeout = -1,                // timeout value in milliseconds, -1 for blocking
				int write_timeout = -1                // timeout for writes in milliseconds, -1 for blocking
			);

			~serial_port();

			// Check ok() after creation of the serial port object.
			// If ok() return false, there's not much you can do except destroy the object and try again.
			bool ok() { return _open && _configured; }

			// Write nbr_chars, or less if timeout occurs
			// Return the number of written chars.
			int write(const char* buffer, int nbr_chars);

			// Read nbr_chars_wanted, or less if timeout occurs
			// Return the number of read chars.
			int read(char* buffer, int nbr_chars_wanted);

			// set read timeout in milliseconds. -1 means blocking
			void set_read_timeout(int timeout);

			// set write timeout, in milliseconds, -1 means blocking
			void set_write_timeout(int timeout);
		};
	}
}

#endif //SERIAL_PORT_HPP
