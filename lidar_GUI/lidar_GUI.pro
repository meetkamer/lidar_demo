#-------------------------------------------------
#
# Project created by QtCreator 2018-01-31T10:13:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lidar_GUI
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp

HEADERS += \
        mainwindow.h

FORMS += \
        mainwindow.ui

LIBS += User32.lib dxgi.lib d3d12.lib d3dcompiler.lib

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../src/bin/x64/Release-Lib/ -lD3D12nBodyGravity
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../src/bin/x64/Debug-Lib/ -lD3D12nBodyGravity

win32:CONFIG(release, debug|release): DEPENDPATH += -$$PWD/../src/bin/x64/Release-Lib/D3D12nBodyGravity.lib
else:win32:CONFIG(debug, debug|release): DEPENDPATH += -$$PWD/../src/bin/x64/Debug-Lib/D3D12nBodyGravity.lib

