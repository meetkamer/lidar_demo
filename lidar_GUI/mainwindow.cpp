#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{    
    on_horizontalSlider_valueChanged(50);
    on_horizontalSlider_2_valueChanged(50);
    on_horizontalSlider_3_valueChanged(50);
    on_horizontalSlider_4_valueChanged(50);
    on_horizontalSlider_5_valueChanged(50) ;
    lidar_lib_begin_thread();
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

extern float lidar_rotation_speed;
extern float lidar_xrange;
extern float lidar_yrange;
extern float lidar_zrange;
extern float lidar_speed;

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    lidar_rotation_speed  = float(value) / 100.0;
}

void MainWindow::on_horizontalSlider_2_valueChanged(int value)
{
    lidar_xrange  = float(value) / 100.0;
}

void MainWindow::on_horizontalSlider_3_valueChanged(int value)
{
    lidar_yrange  = float(value) / 100.0;
}

void MainWindow::on_horizontalSlider_4_valueChanged(int value)
{
    lidar_zrange  = float(value) / 100.0;
}

void MainWindow::on_horizontalSlider_5_valueChanged(int value)
{
    lidar_speed = float(value) / 100.0;
}
