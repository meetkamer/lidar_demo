'''
convert coordinate files to las
[pip install laspy]
 files should be placed in C:\lidar_demo\
'''
import time
import shutil
import laspy
import numpy as np


def make_las_file(path_to_particle_file, path_to_las_file):
    X, Y, Z, R, G ,B  = [], [], [], [], [], []
    for line in open(path_to_particle_file, "rb"):
        #line = line.decode('latin-1')
        x,y,z,r,g,b = [float(d) for d in line.split(',')]
        if x*x+y*y*1.6+z*z > 2300*2300:
            continue
        
        X.append(x)
        Y.append(y)
        Z.append(z)
        kleur = (max(min(y/8,255),0))/255
        r = 255*max(0,np.cos(2*3.1415*kleur))
        g = 255*max(0,np.sin(2*3.1415*kleur))
        R.append(r)
        G.append(g)
        B.append(np.sqrt(255*255-g**2-r**2))
        
    hdr = laspy.header.Header()
    print "writing", path_to_las_file
    outfile = laspy.file.File(path_to_las_file, mode="w", header=hdr)
    allx = np.array(X) # Four Points
    ally = np.array(Y)
    allz = np.array(Z)
    alleRed = np.array(R)
    alleGreen = np.array(G)
    alleBlue = np.array(B)
    
    xmin = np.floor(np.min(allx))
    ymin = np.floor(np.min(ally))
    zmin = np.floor(np.min(allz))

    outfile.header.offset = [xmin,ymin,zmin]
    outfile.header.scale = [0.001,0.001,0.001]

    outfile.x = allx
    outfile.y = ally
    outfile.z = allz
    outfile.red = alleRed
    outfile.green = alleGreen
    outfile.blue = alleBlue
    
    outfile.close()


#path_to_particle_file = r"C:\lidar_demo\particle_files\particles-00-26-15-53-53.txt"
#path_to_las_file = "C:\lidar_demo\particle_files\particles-00-26-15-53-53.las"
#make_las_file(path_to_particle_file, path_to_las_file)

if __name__ == '__main__':
    path_to_particle_file = r"C:\lidar_demo\particle_files\particles-00-31-13-14-01.txt"
    path_to_las_file = r"C:\lidar_demo\particle_files\test_%d.las"%int(10*time.time())
    make_las_file(path_to_particle_file, path_to_las_file)
    from uploader import upload
    upload(path_to_las_file)
   
    