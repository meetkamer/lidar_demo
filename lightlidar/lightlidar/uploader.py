'''
Uploads a file to a folder on the server
'''

import requests

UPLOAD_URL = 'http://lichtfestival.intec.ugent.be/receive_las.php'
def upload(file_path):
    '''
    Upload a las file to the remote web server.. 
    '''
    f = open (file_path, 'rb') # must be binary read
    
    r =  requests.post(url=UPLOAD_URL, data =  {'title':'test_file'},  files = {'file':f})
    
    print r.status_code
    
    print r.headers
    print 'upload complete'
