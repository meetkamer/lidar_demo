
import random
import uuid
import os
import sys
import shutil


def add_suffix(filename, suffix='', ext=None):
    ''' Add suffix to and change extension of filename

    Parameters
    -----------
    filename: string
        full path to file
    suffix: string
        suffix to append to orginal filename
    ext: string
        If not None changes the extension of the filename

    Returns
    --------
    s: string
    	string consisting of the filename with a new suffix and/or extension
    '''
    file_parts = filename.rsplit('.', 1)
    original_filename = file_parts[0]
    extension=file_parts[1]
    if ext:
        extension = ext

    return "{0}_{2}.{1}".format(original_filename, extension, suffix)

NAMES = ["Ferne", 
         "Lizbeth", 
         "Toshia" ,
         "Norberto",
         "Elena" ,
         "Virgil",
         "Lilliam" ,
         "Luella",
         "Florinda",
         "Albertina",
         "Shayna",
         "Shirl",
         "Zandra",
         "Myung",
         "Linda",
         "Yuri",
         "Akilah",
         "Annabell",
         "Tasha",
         "Maybell",
         "Aurore",
         "Iva",
         "Elyse",
         "Debi",
         "Susana",
         "Jann",
         "Isabell",
         "Naida",
         "Gaye",
         "Roselia",
         "Eileen",
         "Temeka",
         "Charley",
         "Rachal",
         "Erich",
         "Tegan" ]

if __name__ == '__main__':
    n = 1
    LAS_DIR = './las/'
    original_file = 'particles-00-26-15-53-64.las'
    for i in range(n):
        name = random.choice(NAMES)
        suffix = str(uuid.uuid4())
        file_path = os.path.join(LAS_DIR, "{}_{}.las".format(name, suffix))
        shutil.copy(original_file, file_path)
        
