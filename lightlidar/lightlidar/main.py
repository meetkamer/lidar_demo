'''
Main file.. includes a daemon

pip install watchdog before use...
Example usage - http://brunorocha.org/python/watching-a-directory-for-file-changes-with-python.html
'''

import sys
import time
from uploader import upload
#import xmltodict
#import magicdate
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from util import add_suffix
from converter import make_las_file
import time

class MyHandler(PatternMatchingEventHandler):
    patterns=["*.txt"] #process only las files

    def process(self, event):
        """
        event.event_type
            'modified' | 'created' | 'moved' | 'deleted'
        event.is_directory
            True | False
        event.src_path
            path/to/observed/file
        """
        print event.src_path
        time.sleep(2)
        #call make las file
        ##TODO: call to make las file
        las_file_name = add_suffix(event.src_path, ext='las')
        make_las_file(path_to_particle_file= event.src_path, path_to_las_file=las_file_name)
        
        #call webserver to upload the file
        upload(las_file_name)


    #def on_modified(self, event):
        #self.process(event)

    def on_created(self, event):
        self.process(event)


if __name__ == '__main__':
    args = sys.argv[1:]
    #ROOT_PATH = 'C:/Users/akatumba/Documents/dev/python/lightlidar/lightlidar/las'
    ROOT_PATH = 'C:/lidar_demo/particle_files'
    observer = Observer()
    observer.schedule(MyHandler(), path=args[0] if args else ROOT_PATH)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()